.. ce_employment documentation master file, created by
   sphinx-quickstart on Wed Sep 29 21:56:17 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to ce_employment's documentation!
=========================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   README.rst
   LICENSE.rst

Computation Library
==========================================

.. automodule:: main

Requirements
-------------

In order to run the computation, we need to install the ``pymrio``
library. The installation of this library will install all other required
libraries like ``pandas``.

.. include:: requirements.txt
   :literal:

Computation Modules
==========================================

.. autofunction:: main.get_bu_data
.. autofunction:: main.downscale_Z_Y
.. autofunction:: utils.io.iot_exp
.. autofunction:: main.get_index
.. autofunction:: main.calc_A_L
.. autofunction:: main.get_mid
.. autofunction:: main.get_ymask
.. autofunction:: main.get_results

