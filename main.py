#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Supplementary material for the paper:
TRACKING A CIRCULAR ECONOMY TRANSITION THROUGH JOBS:
Method development and application in two cities.

This module demonstrates the computational steps described in the above-mentioned paper.

Example:

    The module contains a set of functions used for the computation of city
    level circularity. Each function aims to represent one of the computational
    steps described in the paper.  The central function called `compute_cuty()`
    will call all other functions for the estimation of circularity. The result
    presented on the paper will be run by calling the module on command line::

        $ python main.py.py

    Nonetheless, the function can be run independently of the given examples on
    the paper. A Simple example on how to use the `compute_city()` function can
    be seen below::

        >>> from ce_employment.main import compute_city
        >>> country = 'BRA'     # EORA defined ISO3 country acronym
        >>> city = 'recife'     # City name, will try to read file: `data/empl_recife.csv`
        >>> mid_method = 'cba'  # Method used for the computation of MID, can be either:
        >>>                     # `'cba'` (consumption based accounting) or
        >>>                     # `'pba'` (production based accounting)
        >>>
        >>> results = compute_city(country, city, mid_method)

Todo:
    * (1) Remove warning filters, an update of pymrio is required.
    * (2) Do not load EORA data each time we call this script.
    * (3) Internalize input data manipulation step or make call to public DB.

"""

import time
import pandas as pd
import pymrio

from pymrio.tools.iomath import calc_A, calc_L

from utils.io import downscale_Z, downscale_Y, iot_exp
from utils.plots import plot_matrix

# TODO (1) not a good idea, waiting for a pymrio update
import warnings
warnings.filterwarnings("ignore")

init_start_time = time.time()

EORA_PATH = "./data/eora"
"""str: [`./data/eora`] Relative storage EORA data path."""

# TODO (2) this has to be loaded before calling the function at a higher level
# Load EORA data
print('{:<30}'.format('Load EORA data'), end='\t')
io = pymrio.parse_eora26(year=2013, path=EORA_PATH)
"""pymrio.core.mriosystem.IOSystem: EORA IO Table."""
io.calc_all()
print('@ {:0.2f}'.format(time.time() - init_start_time))


def get_bu_data(city):
    """(1) bottom up data

    Load and prepare city-level employment data.

    This function searches for a file under path `./data/empl_{}`
    with a constructed file name based on the name of the city.
    An input city name like 'recife' would construct the following file name and path: `./data/empl_rec.csv`.

    Note that this currently assumes mappings and scaling data are already part of the dataset.
    We want to generalize this in the near future.

    Args:
        city (str): Name of the city.

    Returns:
        re_ce_empl (pandas.core.frame.DataFrame): DataFrame containing all city-level relevant data.
        empl_p (pandas.core.series.Series): Per-Capita city employment share.

    Examples:

        >>> re_ce_empl, empl_p = get_bu_data("recife")
        >>> print(re_ce_eml)
        >>> print(empl_p)

    """
    empl_data = "./data/empl_{}.csv".format(city[0:3])
    city_data = pd.read_csv(empl_data)

    # TODO (3) This step requiers additional manipulation of input data.
    region_empl = city_data.copy()
    region_empl = region_empl.drop_duplicates(['eora_name']).set_index("eora_name")
    empl_p = region_empl["city_ppn"]

    # Make a copy of data frame and rename columns.
    city_data.set_index("eora_name", inplace=True)
    re_ce_empl = city_data.copy()
    re_ce_empl["io_code"] = re_ce_empl["newempcode"]
    re_ce_empl["empl"] = re_ce_empl["city_employment"]
    re_ce_empl["empl_p"] = re_ce_empl["city_sector_ppn"]

    return re_ce_empl, empl_p


def downscale_Z_Y(Z_in, Y_in, empl_p):
    """(2) Downscale IO

    Downscaled IO tables

    .. math::
        Z = Z * emp_{ppn}

    .. math::
        Y = Y * emp_{ppn}

    Args:
        Z_in (pandas.core.frame.DataFrame):
        Y_in (pandas.core.series.Series):
        empl_p (pandas.core.series.Series):

    Returns:
        Z_out (pandas.core.frame.DataFrame):
        Y_out (pandas.core.series.Series):

    Examples:

        >>> Z =
        >>> Z, Y = downscale_Z_Y(Z, Y, emp)
        >>> print(Z)
        >>> print(Y)

    """

    Z_out = downscale_Z(Z_in, empl_p)
    Y_out = downscale_Y(Y_in, empl_p)
    return Z_out, Y_out


def get_index(midx):
    """(4) Index IO

    Construct boolean vectors for selection of elements within the IO tables.
    Called index as these manipulate pandas.DataFrames

    The construction of such vectors are based on name patters defined on the city-level input files,
    which contain employment shares by economic sectors.

    Args:
        midx (pandas.core.series.Series): Expanded IO index matrix.

    Returns:
        core_mask (pandas.core.series.Series): Selected IO index matrix containing the character `.C`.
        enabling_mask (pandas.core.series.Series): Selected IO index matrix containing the character `.E`.
        indirectly_mask (pandas.core.series.Series): Selected IO index matrix containing the character `.I`.

    Examples:
        >>> m = pd.Series({'1.C':1, '2.E':2, '3.I':3})
        >>> cm, em, im = get_index(m)
        >>> print(cm)
        >>> print(em)
        >>> print(im)

    """
    core_mask = midx.str.contains(".C", regex=False)
    enabling_mask = midx.str.contains(".E", regex=False)
    indirectly_mask = midx.str.contains(".I", regex=False)
    return core_mask, enabling_mask, indirectly_mask


def calc_A_L(Z, Y):
    """(5) A, L matrix

    Creating A, L matrices

    Calculate the $A$ matrix (coefficients) from $Z$ and $x$

    .. math::
        A = Z * ( 1 \div ( \sum{Z} + Y ))

    Calculate the Leontief $L$ from $A$.

    .. math::
        L = (I - A )^{-1}

    Args:
        Z ():
        Y ():

    Returns:
        A ():
        L ():
        outpu_x ():

    Examples:
        >>> Z =
        >>> Y =
        >>> A, L, x = calc_A_L(Z, Y)

    """
    # Sum over the rows
    output_x = Z.sum(axis=1) + Y
    A = calc_A(Z, output_x)
    L = calc_L(A)
    return A, L, output_x


def get_mid(Q, re_ce_empl, country, city, mid_method):
    """(6) MID

    .. math::
        MID = 1 - ( MID_{imp} \div ( MID_{cba} + MID_{exp} ))

    Args:
        Q (pymrio.core.mriosystem.Extension): EORA IO Environmental extension.
        re_ce_empl (pandas.core.frame.DataFrame): DataFrame containing all city-level relevant data.
            Requierd in order to expand the original IO matrix to city-level
        country (str): Country ISO-3 name.
        city (str): City name.
        mid_method (str): Method to be used for the computation of MID.
            Options are: "cba" (consumption) or "pba" (production).

    Returns:
        mid_exp (pandas.core.frame.DataFrame):

    Examples:
        >>> Q =
        >>> re_ce_empl =
        >>> mid = get_mid(Q, re_ce_empl, "BRA", "recife", "cba")
        >>> print(mid)

    """
    # Extract data from EORA IO
    # inx = ['Raw material inputs, total' in i[1] for i in io.Q.D_imp.index]
    inx = ['Raw material inputs' in i[0] and i[1] == 'Total' for i in Q.D_imp.index]
    plot_matrix(1 - (Q.D_imp.loc[inx, country] / (Q.D_cba.loc[inx, country] + Q.D_imp.loc[inx, country])),
                save=True, fig_name="{}-{}_MID".format(country, city),
                figsize=(15, 5),
                title="Material Import Dependency (MID)",
                colorbar=True,
                vmin=0.4, vmax=1
                )

    imp = Q.D_imp.loc[inx, country].sum()
    cba = Q.D_cba.loc[inx, country].sum()
    pba = Q.D_pba.loc[inx, country].sum()

    # MID DataFrame
    mid = pd.concat([imp, cba, pba], axis=1, keys=["imp", "cba", "pba"])

    # Consumption based
    mid["cmid"] = mid["imp"] / (mid[mid_method] + mid["imp"])
    mid.loc[mid["cmid"] == float('inf'), 'cmid'] = 0

    # Take the inverse (high dependency is bad)
    mid["mid"] = 1 - mid["cmid"]

    # Apply 0 final demand to mining and quarrying as it is linear industry
    mid.loc["Mining and Quarrying"] = 0

    # Expand to our sectors
    mid_exp = re_ce_empl.join(mid, how="left")
    mid_exp.set_index("io_code", inplace=True)
    mid_exp = mid_exp["mid"]

    return mid_exp


def get_ymask(Z, core_mask, enabling_mask, indirectly_mask, mid_exp):
    """(7) Apply Mask

    Generate a mask (:math:`M`). Sum over the inputs (core prop). This is equal to:

    +--------------+--------+------------+------------+
    |              | core   | enabling   | indirectly |
    +==============+========+============+============+
    | core         | -      | -          | 0.x        |
    +--------------+--------+------------+------------+
    | enabling     | 0.x    | -          | -          |
    +--------------+--------+------------+------------+
    | indirectly   | -      | -          | -          |
    +--------------+--------+------------+------------+

    .. math::
        M_{core} = 0

    .. math::
        M_{enab} = \sum{M_{core}}_m^j \div \sum{M}_m^j

    .. math::
        M_{indi} = (\sum{M_{core}}_n^i + \sum{M_{enab}}_n^i) \div \sum{M}_n^i

    .. math::
        M_{indi} = M_{indi} * MID

    .. math::
        M = M_{core} + M_{enab} + M_{indi}

    Where:
        1) :math:`core`: core_mask,
        2) :math:`enab`: enabling_mask,
        3) :math:`indi`: indirectly_mask,
        4) :math:`MID`: mid_exp, Material import dependency multiplier.

    Args:
        Z (pandas.core.frame.DataFrame): IO matrix.
        core_mask (pandas.core.series.Series):
        enabling_mask (pandas.core.series.Series):
        indirectly_mask (pandas.core.series.Series):
        mid_exp (pandas.core.series.Series):

    Returns:
        M (pandas.core.series.Series):

    Examples:
        >>> Z =
        >>> cm =
        >>> em =
        >>> im =
        >>> mid =
        >>> M = get_ymask(Z, cm, em, im, mid)
        >>> print(M)

    """
    Y_mask_core = Z.apply(lambda row: 0, axis=1)  # Not used
    Y_mask_core[~core_mask.values] = 0

    Y_mask_enabling = Z.apply(lambda row: row[core_mask.values].sum() / row.sum(), axis=1).fillna(0)
    Y_mask_enabling[~enabling_mask.values] = 0

    # Indirectly sectors have circular final demand to the extent they use core or enabling products AND local materials
    Y_mask_indirectly = Z.apply(lambda col: (col[core_mask.values].sum() + col[enabling_mask.values].sum()) / col.sum(), axis=0).fillna(0)
    Y_mask_indirectly[~indirectly_mask.values] = 0

    # Multiply all indirect activities by their corresponding MID multiplier
    Y_mask_indirectly_mid = Y_mask_indirectly * mid_exp

    # Overlay to make a single mask
    M = Y_mask_core + Y_mask_enabling + Y_mask_indirectly_mid
    #M.loc[Y_mask.index.str.contains(".I.", regex=False)] = 0

    return M


def get_results(re_ce_empl, L, dx, output_x):
    """(8) Results

    1. Generate filter masks.
    2. Calculate employment intensity vector.

    Divide $dx$ over original $x$ to get the proportions.

    .. math::
        y_{mp} = dx \div OUT_x
        EMP_n = y_{mp} * EMP_{city}

    Args:
        re_ce_empl (pandas.core.frame.DataFrame):
        L (pandas.core.frame.DataFrame): Leontief L
        dx (pandas.core.frame.DataFrame):
        output_x (pandas.core.frame.DataFrame):

    Returns:
        results (pandas.core.frame.DataFrame):

    Examples:
        >>> re_ce_empl
        >>> L =
        >>> dx =
        >>> x =
        >>> results = get_results(re_ce_empl, L, dx, x)
        >>> print(results)

    """

    emp_city = re_ce_empl["empl"]
    emp_city.index = L.index
    # Cjob calc strategies
    results = [emp_city]

    y_mp = dx / output_x
    emp_new = y_mp * emp_city
    emp_new.name = "cjobs"
    results.append(emp_new)
    return results


def compute_city(country, city, mid_method, verbose=False, store_steps=False):
    # #################################
    # (0) Prepare data for computation
    # #################################
    if verbose: print('computing {}/{} with method: {}'.format(country, city, mid_method)); start_time = time.time()
    # Prepare the tables we care about.
    Z_in = io.Z.loc[country, country]
    Y_in = io.Y.loc[country, country].sum(axis=1)
    plot_matrix(
        Z_in, save=True,
        title="Input EORA IO table",
        fig_name="{}-{}_inputIO".format(country, city))

    # #################################
    # (1) Load prepped bottom up data sheet
    # #################################
    if verbose: print('{:<30}'.format('\t(1) bottom up data'), end="\t"); start_time = time.time()
    re_ce_empl, empl_p = get_bu_data(city)
    print('@ {:0.2f}'.format(time.time() - start_time))

    # #################################
    # (2) Downscale IO table
    # #################################
    if verbose: print('{:<30}'.format('\t(2) Downscale IO'), end="\t"); start_time = time.time()
    Z_out, Y_out = downscale_Z_Y(Z_in, Y_in, empl_p)
    print('@ {:0.2f}'.format(time.time() - start_time))

    # #################################
    # (3) Expand IO table
    # #################################
    if verbose: print('{:<30}'.format('\t(3) Expand IO'), end="\t"); start_time = time.time()
    Z, Y = iot_exp(Z_out, Y_out, re_ce_empl)
    plot_matrix(
        Z,
        save=True,
        title="Expanded IO table",
        fig_name="{}-{}".format(country, city),
        lim=7, log=True, #colorbar=True
    )
    print('@ {:0.2f}'.format(time.time() - start_time))

    # #################################
    # (4) Create IO Index
    # #################################
    if verbose: print('{:<30}'.format('\t(4) Index IO'), end="\t"); start_time = time.time()
    core_mask, enabling_mask, indirectly_mask = get_index(re_ce_empl["io_code"])
    print('@ {:0.2f}'.format(time.time() - start_time))

    # #################################
    # (5) Compute A and L matrix
    # #################################
    if verbose: print('{:<30}'.format('\t(5) A, L matrix'), end="\t"); start_time = time.time()
    A, L, output_x = calc_A_L(Z, Y)
    print('@ {:0.2f}'.format(time.time() - start_time))

    # #################################
    # (6) MIDs
    # #################################
    if verbose: print('{:<30}'.format('\t(6) MID'), end="\t"); start_time = time.time()
    mid_exp = get_mid(io.Q, re_ce_empl, country, city, mid_method)
    print('@ {:0.2f}'.format(time.time() - start_time))

    # #################################
    # (7) Apply Mask
    # #################################
    if verbose: print('{:<30}'.format('\t(7) Apply Mask'), end="\t"); start_time = time.time()
    Y_mask = get_ymask(Z, core_mask, enabling_mask, indirectly_mask, mid_exp)
    # """
    # dx = L @ \left(Y * M \right)
    # """
    dx = L @ (Y * Y_mask)
    print('@ {:0.2f}'.format(time.time() - start_time))

    # #################################
    # (8) Results
    # #################################
    if verbose: print('{:<30}'.format('\t(8) Results'), end="\n"); start_time = time.time()
    results = get_results(re_ce_empl, L, dx, output_x)
    print('@ {:0.2f}'.format(time.time() - start_time))

    # #################################
    # (9) Return
    # #################################
    return pd.concat(results, axis=1)


def main():
    verbose = True; store_steps = True
    if verbose: print("\n--------VERBOSE--------\n")
    mid_method = 'cba'

    for country in ['BRA', 'BEL']:
        for city in ['recife', 'brussels']:

            result = compute_city(country, city, mid_method, verbose=verbose, store_steps=store_steps)
            results_path = "./tmp/{}_{}_out_{}.csv".format(country, city, mid_method)
            if verbose:
                print('='*30)
                print(result.head())
                print('-' * 30)
                print(result.sum(axis=0))
                print('=' * 30)
                print("{}-{} - {:0.2%} Circular".format(
                    country, city.title(), result.cjobs.sum() / result.empl.sum()))
                print('=' * 30)
                print("\n\n")
            result.to_csv(results_path)

    if verbose: print('\nFINAL TIME: {:0.2f}'.format(time.time() - init_start_time))


if __name__ == "__main__":
    main()
