import psycopg2
import pandas as pd

DEC2FLOAT = psycopg2.extensions.new_type(
    psycopg2.extensions.DECIMAL.values,
    'DEC2FLOAT',
    lambda value, curs: float(value) if value is not None else None)
psycopg2.extensions.register_type(DEC2FLOAT)


CONN_URI = "postgres://localhost:5436/jobs"


def get_nace_eora_mapping():
    #                                    io_code
    # eora_name
    # Agriculture                            A01
    # Agriculture                            A02
    # Fishing                                A03
    # Mining and Quarrying                   B05
    # Mining and Quarrying                   B06
    # ...                                    ...
    # Education, Health and Other Services   S95
    # Education, Health and Other Services   S96
    # Private Households                     T97
    # Others                                 T98
    # Re-export & Re-import                  U99

    # [89 rows x 1 columns]

    with psycopg2.connect(CONN_URI) as conn:
        with conn.cursor() as cur:
            cur.execute("""
                select nace3 as io_code, eora as eora_name
                from nace_eora;
            """)

            nace_eora = pd.DataFrame(cur.fetchall(), columns=[_[0] for _ in cur.description])
            nace_eora.set_index("eora_name", inplace=True)

            return nace_eora


def get_country_region_employment(region='BE100'):
    #                                                     nuts3            city_empl            country_empl
    # eora_name
    # Agriculture                                         BE100  0.10000000000000004    59.50000000000001402
    # Construction                                        BE100   20.099999999999959    272.1999999999999891
    # Education, Health and Other Services                BE100   243.16787050341801   1363.0923621210940968
    # ...                                                 ...                    ...                     ...
    # Transport Equipment                                 BE100   4.1765145848915522     45.1041004164951869
    # Wholesale Trade                                     BE100     23.8177453665455     218.610633844171914
    # Wood and Paper                                      BE100    2.448267265021194     41.9047562106424277

    # [25 rows x 4 columns]

    with psycopg2.connect(CONN_URI) as conn:
        with conn.cursor() as cur:
            cur.execute("""
                select eora as eora_name, nuts3, city_empl, country_empl, coalesce(city_empl / nullif(country_empl, 0), 0) as empl_p
                from (
                    select
                        eora.eora,
                        empl.nuts3,
                        sum(empl.nuts3_nace3_empl_est) as city_empl,
                        sum(sum(empl.nuts3_nace3_empl_est)) over (partition by empl.nuts0, eora.eora) as country_empl


                    from nuts3_nace3_employment_estimates empl
                    join nace_eora as eora on (empl.nace3 = eora.nace3)

                    where empl.nuts0 = 'BE'

                    group by empl.nuts0, eora.eora, empl.nuts3
                    order by 1, 2
                ) _
            """)

            country_region_empl = pd.DataFrame(cur.fetchall(), columns=[_[0] for _ in cur.description])
            country_region_empl.set_index("eora_name", inplace=True)

            # filter our region
            country_region_empl = country_region_empl[country_region_empl["nuts3"]==region]

            return country_region_empl

def get_region_circular_employment():
    #               io_code         empl    empl_p
    # eora_name
    # Agriculture     A01.C     0.000000  0.000000
    # Agriculture     A01.E     0.000000  0.000000
    # Agriculture     A01.I     0.096014  0.960139
    # Agriculture     A02.C     0.000000  0.000000
    # Agriculture     A02.E     0.000000  0.000000
    # ...               ...          ...       ...
    # Wood and Paper  C17.E     0.000000  0.000000
    # Wood and Paper  C17.I     0.442782  0.180855
    # Wood and Paper  C18.C     0.000000  0.000000
    # Wood and Paper  C18.E     0.000000  0.000000
    # Wood and Paper  C18.I     1.606582  0.656212

    # [261 rows x 3 columns]

    with psycopg2.connect(CONN_URI) as conn:
        with conn.cursor() as cur:
            cur.execute("""
                select eora as eora_name, nace3 as io_code, ce_empl_est as empl, coalesce(ce_empl_est / nullif(eora_empl_est, 0), 0) as empl_p
                from (
                    select
                        eora,
                        nace3,
                        ce_empl_est,
                        sum(ce_empl_est) over (partition by eora) as eora_empl_est

                    from nuts3_nace3_ce_empl_est

                    where nuts3 = 'BE100'
                ) _
                order by 1, 2

            """)

            re_ce_empl = pd.DataFrame(cur.fetchall(), columns=[_[0] for _ in cur.description])
            re_ce_empl.set_index("eora_name", inplace=True)

            return re_ce_empl


