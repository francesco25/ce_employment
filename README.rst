=============================
How to reproduce the results?
=============================

Clone the repository
======================

.. code-block:: bash

  git clone git@gitlab.com:UNEnvironmentEconomy/ce_employment.git

  cd ce_employment

Data
======================

**Employment data**
Because of licence of the input dataset we are not allowed to upload the require input datasets to the repository.
Pleae drop us a line to provide you with the required data-sets.

**IO data**
We utilise 2013 eora data which can be downloaded [here](https://www.worldmrio.com/eora26/). Store this data in your ce_employment folder in a new folder called ./data/eora

**Results**
Create a folder called ./tmp in your ce_employment folder where the results will be stored

Create a python virtual environment
======================================

.. code-block:: bash

   virtualenv --python=python3.9 env

Enter the virtual environment and install requirements with pip
================================================================

.. code-block:: bash

  source env/bin/activate

  pip install -r requirements.txt

Run the main script
===================

.. code-block:: bash

  python main.py

You should see the following output
===================================

.. code-block:: bash

  Load EORA data                	@ 19.24

  --------VERBOSE--------

  computing BRA/recife with method: cba
    (A) bottom up data           	@ 0.01
    (B) MID                      	@ 0.01
    (C) Downscale IO             	@ 0.01
    (D) Index IO                 	@ 0.00
    (E) A, L matrix              	@ 0.19
    (F) Mask                     	@ 0.00
    (G) Apply Mask               	@ 0.11
    (H) Results                  	@ 0.00
  ==============================
           empl       cjobs
  io_code
  1.1.I    2029  219.683682
  1.2.I      94   10.177558
  1.3.I     434   46.990004
  1.5.I     291   31.507122
  1.6.I     553   59.874360
  ------------------------------
  empl     878430.000000
  cjobs     73300.968511
  dtype: float64
  ==============================
  BRA-Recife - 8.34% Circular
  ==============================



  computing BRA/brussels with method: cba
    (A) bottom up data           	@ 0.01
    (B) MID                      	@ 0.01
    (C) Downscale IO             	@ 0.02
    (D) Index IO                 	@ 0.00
    (E) A, L matrix              	@ 0.84
    (F) Mask                     	@ 0.00
    (G) Apply Mask               	@ 0.22
    (H) Results                  	@ 0.00
  ==============================
           empl     cjobs
  io_code
  111.I      27  2.350904
  113.I      34  2.960398
  119.I       9  0.783635
  123.I       3  0.261212
  128.I       3  0.261212
  ------------------------------
  empl     714200.000000
  cjobs    100953.901507
  dtype: float64
  ==============================
  BRA-Brussels - 14.14% Circular
  ==============================



  computing BEL/recife with method: cba
    (A) bottom up data           	@ 0.01
    (B) MID                      	@ 0.01
    (C) Downscale IO             	@ 0.01
    (D) Index IO                 	@ 0.00
    (E) A, L matrix              	@ 0.09
    (F) Mask                     	@ 0.00
    (G) Apply Mask               	@ 0.11
    (H) Results                  	@ 0.00
  ==============================
           empl       cjobs
  io_code
  1.1.I    2029  153.005811
  1.2.I      94    7.088490
  1.3.I     434   32.727709
  1.5.I     291   21.944155
  1.6.I     553   41.701436
  ------------------------------
  empl     878430.000000
  cjobs     61551.972365
  dtype: float64
  ==============================
  BEL-Recife - 7.01% Circular
  ==============================



  computing BEL/brussels with method: cba
    (A) bottom up data           	@ 0.01
    (B) MID                      	@ 0.01
    (C) Downscale IO             	@ 0.02
    (D) Index IO                 	@ 0.00
    (E) A, L matrix              	@ 0.76
    (F) Mask                     	@ 0.00
    (G) Apply Mask               	@ 0.22
    (H) Results                  	@ 0.00
  ==============================
           empl     cjobs
  io_code
  111.I      27  1.647320
  113.I      34  2.074403
  119.I       9  0.549107
  123.I       3  0.183036
  128.I       3  0.183036
  ------------------------------
  empl     714200.000000
  cjobs     51947.433161
  dtype: float64
  ==============================
  BEL-Brussels - 7.27% Circular
  ==============================




  FINAL TIME: 21.98


